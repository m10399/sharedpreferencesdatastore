package com.example.sharedpreferencedatastorekullanimi

import android.os.Bundle
import android.util.Log
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_main)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
        val ap = AppPref(this)
        val job = CoroutineScope(Dispatchers.Main).launch {
            //ap.kayitAd("Ahmet")
            ap.kayitYas(25)
            ap.kayitBoy(1.70)
            ap.kayitBekar(true)

            val liste = HashSet<String>()
            liste.add("Mehmet")
            liste.add("Zeynep")
            ap.kayitArkadasList(liste)
            ap.silAd()
            val gelenAd = ap.okuAd()
            val gelenYas = ap.okuYas()
            val gelenBoy = ap.okuBoy()
            val gelenBekar = ap.okuBekar()
            val gelenArkadasList : Set<String>? = ap.okuArkadasList()
            Log.e("Gelen Ad",gelenAd)
            Log.e("Gelen Ad",gelenYas.toString())
            Log.e("Gelen Ad",gelenBoy.toString())
            Log.e("Gelen Ad",gelenBekar.toString())

            for (arkadasList in gelenArkadasList!!){
                Log.e("Gelen arkadas",arkadasList)
            }
        }
    }
}